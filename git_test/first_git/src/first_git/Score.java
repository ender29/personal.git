package first_git;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Score {

	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		// 利用file读取html文件 再通过jsoup解析html
		File input1 = new File("../tmp/small.html");
		Document small = Jsoup.parse(input1,"UTF-8","https://www.mosoteach.cn/web/index.php?c=interaction&m=index&clazz_course_id=CD7AE281-4AF8-11EA-9C7F-98039B1848C6");
		Elements rows1 = small.getElementsByClass("interaction-row"); // 第一层 div
		File input2 = new File("../tmp/all.html");
		Document all = Jsoup.parse(input2,"UTF-8","https://www.mosoteach.cn/web/index.php?c=interaction&m=index&clazz_course_id=8AF72060-4C93-11EA-9C7F-98039B1848C6");
		Elements rows2 = all.getElementsByClass("interaction-row");
		
		// 创建经验点统计量
		int exp[] = new int[5];
		int exps[] = new int[5];
		find(rows1,exp);
		find(rows2, exp);
		
	    // 计算结果,读取配置文件
		Properties pro = new Properties();
		InputStream in = Score.class.getClassLoader().getResourceAsStream("total.properties");
		pro.load(in);
		
		exps[0] = Integer.parseInt(pro.getProperty("before"));
		exps[1] = Integer.parseInt(pro.getProperty("base"));
		exps[2] = Integer.parseInt(pro.getProperty("test"));
		exps[3] = Integer.parseInt(pro.getProperty("program"));
		exps[4] = Integer.parseInt(pro.getProperty("add"));
		double gredit=0;
		gredit += exp[0]*1.0/exps[0]*100*0.25;
		gredit += exp[1]*1.0/exps[1]*95*0.3;
		gredit += exp[2]*1.0/exps[2]*100*0.2;
		gredit += exp[3]*1.0/exps[3]*95*0.1;
		gredit += exp[4]*1.0/exps[4]*90*0.05;
		int t = (int)(gredit*100);
		System.out.println((double)t/100);
	}
	
	
	private static void find(Elements rows,int exps[]) {
		// 正则表达式解析dom
		String regex1 = "[^0-9：-]";
		String regex2 = "\\d+";
		Pattern pattern1 = Pattern.compile(regex1);
		Pattern pattern2 = Pattern.compile(regex2);
		for (Element row : rows) {
			Element names = row.select("span.interaction-name").first(); // 第二层 span
			String name = names.attr("title");
			Matcher matcher = pattern1.matcher(name);
			// 验证是否能够找到每种作业类型，以便于后续的统计
			String rename = "";
			while(matcher.find()) {			
				rename += matcher.group();
			}
//			System.out.println(rename);
			// 第三层 div
			Element div = row.getElementsByAttributeValue("style", "float:left;").first();
			
			// 获取经验以及互动经验的加成
			Elements fexps = div.getElementsByTag("span");
			int e=0;
			int num = 0;
			for (Element fexp : fexps) {
				if(!(fexp.attr("style").equals("color:#8FC31F;"))) {
					continue;
				}
				matcher = pattern2.matcher(fexp.text());
				while(matcher.find()) {
					num=Integer.parseInt(matcher.group());
					e+=num;
				}
			}
			if(rename.indexOf("课前自测")!=-1) {
				exps[0]+=e;
			}
			if(rename.indexOf("课堂完成")!=-1) {
				exps[1]+=e;
			}
			if(rename.indexOf("课堂小测")!=-1) {
				exps[2]+=e;
			}
			if(rename.indexOf("编程题")!=-1) {
				exps[3]+=e;
			}
			if(rename.indexOf("附加题")!=-1) {
				exps[4]+=e;
			}
		}
	}
}
